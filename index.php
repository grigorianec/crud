<?php

require('db_connect.php');

try {
	$list = $pdo->query('SELECT * FROM list');
	if (!$list) {
	print_r($pdo->errorInfo());
	die;
}
	$listArray = $list->fetchAll();

} catch (Exception $e) {
	echo 'Cannot select hotels';
	die;
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="main/main.css">
</head>
<body>
	<?php include 'header.php'?>
	<section>
		<div class="container main">
			<div class="row">
				<div class="col">
					<h1>
						Данные администраторов, преподователей и студентов!
					</h1>
				</div>
			</div>
			<div class="row">
				<div class="col mr-auto ml-auto">
					<table border="1" class="table">
						<thead class="thead-dark">
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Ф.И.О</th>
								<th scope="col">Телефон</th>
								<th scope="col">Email</th>
								<th scope="col">Группа</th>
								<th scope="col"></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($listArray as $lis) : ?>
								<tr>
									<td>
										<?=$lis['id']?>.
									</td>
									<td>
										<?=$lis['fullName']?>
									</td>	
									<td>
										<?=$lis['phone']?>
									</td>
									<td>
										<?=$lis['email']?>
									</td>
									<td>
										<?=$lis['role']?>
									</td>
									<?php if ($lis['role'] == 'Студент'): ?>
										<td>
											Средняя оценка -
											<?=$lis['averangeMark']?>
										</td>
									<?php elseif ($lis['role'] == 'Администратор') : ?>
										<td>
											Рабочий день -
											<?=$lis['workingDay']?>
										</td>
									<?php elseif ($lis['role'] == 'Преподователь'): ?>
										<td>
											Предмет - 
											<?=$lis['subject']?>
										</td>
									<?php endif ?>
									<td class="buttons">
										<a class="page-1" href="view.php?id=<?=$lis['id']?>">
											View
										</a>
										<a class="page-1" href="edit.php?id=<?=$lis['id']?>">
											Edit
										</a><br>
										<a class="page-1" href="delete.php?id=<?=$lis['id']?>">
											Delete
										</a>
										<a class="page-1" href="add_to.php">
											Add
										</a>
									</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	<?php include 'footer.php' ?>
</body>
</html>