<?php

if (!isset($_POST['id'])) {
	throw new Exception('ID not passed');
	die;
}

require_once('db_connect.php');

try {

	$sql = 'UPDATE list
		set
			fullName = :fullName,
			phone = :phone,
			email = :email,
			averangeMark = :averangeMark,
			subject = :subject,
			workingDay = :workingDay
		where
			id = :id
	;';

	$s = $pdo->prepare($sql);

		$s->bindValue(':fullName', $_POST['fullName']);
		$s->bindValue(':phone', $_POST['phone']);
		$s->bindValue(':email', $_POST['email']);
		$s->bindValue(':averangeMark', $_POST['averangeMark']);
		$s->bindValue(':subject', $_POST['subject']);
		$s->bindValue(':workingDay', $_POST['workingDay']);
		$s->bindValue(':id', $_POST['id']);

	$s->execute();

	header('Location: view.php?id=' . $_POST['id']);
} catch (Exception $e) {
	echo 'ID not passed';
	die;
}
