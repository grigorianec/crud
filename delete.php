<?php
if (isset($_GET['id'])) {
	require_once('db_connect.php');

	try {
		$sql = 'SELECT * from list where id = :id';
		$s = $pdo->prepare($sql);
		$s->bindValue(':id', (int) $_GET['id']);
		$s->execute();

		$wes = $s->fetchObject();
	} catch (Exception $e) {
		echo 'Unable to select';
	}

} else {
	throw new Exception('ID param missed');
	die;
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Delete</title>
</head>
<body>
	<h1>
		Вы хотите удалить <?=$wes->fullName?>?
	</h1>
	<form action="delete_confirm.php" method="post">
		<input type="hidden" name="id" value="<?=$wes->id?>">
		<button type="submit">Yes</button>
		<a href="index.php">No</a>
	</form>
</body>
</html>