<?php

if (isset($_GET['id'])) {
	require_once('db_connect.php');

	try {

		$sql = 'SELECT * from list where id = :id';
		$s = $pdo->prepare($sql);
		$s->bindValue(':id', (int) $_GET['id']);
		$s->execute();

		$wes = $s->fetchObject();
	} catch (Exception $e) {
		echo 'Unable to select';
	}

} else {
	throw new Exception('ID param missed'); 
	die;
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Update</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="main/main.css">
</head>
<body>
	<?php include 'header.php'?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-6 mr-auto ml-auto">
					<form action="update.php" method="POST">
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Ф.И.О
							</label>
							<input type="text"  name="fullName" class="form-control" id="exampleFormControlInput1" value="<?=$wes->fullName?>" placeholder="Иванов Иван Иванович">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Телефон
							</label>
							<input type="text" name="phone" class="form-control" id="exampleFormControlInput1" value="<?=$wes->phone?>" placeholder="+380934447834">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Email address
							</label>
							<input type="email" name="email" class="form-control" id="exampleFormControlInput1" value="<?=$wes->email?>" placeholder="name@example.com">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Средняя Оценка/Процент посещения
							</label>
							<input type="text" name="averangeMark" class="form-control" id="exampleFormControlInput1" value="<?=$wes->averangeMark?>">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Предмет
							</label>
							<input type="text" name="subject" class="form-control" id="exampleFormControlInput1" value="<?=$wes->subject?>" placeholder="PHP">
						</div>
						<div class="form-group">
							<label for="exampleFormControlInput1">
								Рабочий день
							</label>
							<input type="text" name="workingDay" class="form-control" id="exampleFormControlInput1" value="<?=$wes->workingDay?>" placeholder="Monday">
						</div>
						<input type="hidden" value="<?=$wes->id?>" name="id">
						<button type="submit">
							Изменить
						</button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php include 'footer.php' ?>
</body>
</html>